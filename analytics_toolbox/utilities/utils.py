#!/usr/bin/env python3
"""
Analytic tools utilities for standard implementations.

..author: Oluwatosin Sonuyi <oluwatosinsonuyi@mhsinc.net>
"""
import logging
import itertools
import pandas as pd

logger = logging.getLogger('analytics_toolbox.{}'.format(__name__))

import pdb

def check_persistence(input_series,persistence):
	"""
	check if a series has a number of consecutive truths

	:param input_series: series of values of booleans
	:input_series type: pandas Series

	:param persistence: number of consecutive values
	:persistence type: int

	:returns: True or false if persistence is met
	:rtype: boolean

	:example:
	
	>>> input_s = [True,True,False,True,False,True]
	>>> check_persistence(input_s,4)
	>>> False
	"""

	# make sure the input type is a series
	if isinstance(input_series,pd.DataFrame):
		try:
			input_series = pd.Series(input_series[0])
		except KeyError:
			# this means the dataframe was empty
			return False
	elif isinstance(input_series,list):
		input_series = pd.Series(input_series)
	
	# if input series is empty return False
	if input_series.empty:
		return False

	consecutive_vals = []
	for bool_val, iterval_group in itertools.groupby(input_series):
		"""
		note than any loggers that access the iterval group will break the function
		since the iterval group can only be iterated over once
		"""
		size_of_consecutive_vals = len(list(iterval_group))

		if bool_val:
			consecutive_vals.append(size_of_consecutive_vals)
		else:
			# if the consecutive vals are False, then we dont care
			consecutive_vals.append(0)

	logger.debug("consecutive values {}".format(consecutive_vals))

	return max(consecutive_vals) >= persistence

