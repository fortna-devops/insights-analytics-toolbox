#!/usr/bin/env python3

import pyarrow as pa

PYARROW_SCHEMA_TS = pa.schema([
	pa.field('timestamp', pa.timestamp('ms')),
	pa.field('rms_velocity_x', pa.float64()),
	pa.field('rms_velocity_x_residuals', pa.float64()),
	pa.field('rms_velocity_z', pa.float64()),
	pa.field('rms_velocity_z_residuals',pa.float64()),
	pa.field('temperature', pa.float64()),
	pa.field('temperature_residuals', pa.float64()),
	pa.field('sensor', pa.string()),
	pa.field('conveyor', pa.string()),
	pa.field('equipment_type', pa.string()),
])



PYARROW_SCHEMA_ON_OFF = pa.schema([
	pa.field('timestamp', pa.timestamp('ms')),
	pa.field('conveyor', pa.string()),
	pa.field('conveyor_state', pa.string()),
])
