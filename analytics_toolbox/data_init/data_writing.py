#!/usr/bin/env python3
"""
Module for writing data after calculations in analytics.
..author: Oluwatosin Sonuyi <oluwatosinsonuyi@mhsinc.net>
..author: Coco Wu <Jiewu@mhsinc.net>
"""

import logging
import copy
import s3fs
import json
import csv

import numpy as np
import pandas as pd
import pyarrow as pa
import pyarrow.parquet as pq
import boto3
from botocore.exceptions import ClientError

import pdb

from analytics_toolbox.constants import output_table_schema
from analytics_toolbox.data_init import data_retrieval
from analytics_sdk_core.analytic_exceptions import exceptions
from analytics_sdk_core.data_init import data_write_lite




logger = logging.getLogger('analytics_toolbox.{}'.format(__name__))

class DataWriter(data_write_lite.DataWriterLite):
	"""
	General purpose data pulling from Athena with capabilities for
	retrying failed queries and error handling. Abstracts a few
	commonly used queries such as getting timeseries data and
	the sensor information. Also allows for querying tags across
	multiple tables without having to know where each tag is stored.
	"""

	def __init__(self, s3_bucket_name, boto_session=None):
		"""
		Need to have credentials stored in ~/.aws/credentials
		and region stored in ~/.aws/config
		"""
		# self.s3_client = boto3.client('s3')
		# self.s3_bucket_name = s3_bucket_name
		# # TODO: create s3fs with bucket name
		# #self.s3fs = s3fs.S3FileSystem(self.s3_bucket_name)
		# self.s3fs = s3fs.S3FileSystem()

		super(DataWriter,self).__init__(s3_bucket_name, boto_session)


	def write_parquet_ts_predictions_data(self, df,
										  table_name='timeseries_predictions',
										  dir_name=None):
		"""
		write parquet data to s3 file system from pandas dataframe

		:param df: input data to be written to s3
		:df type: pandas DataFrame

		:param table_name: table in athena where the data will be written
		:table_name type: string

		:param dir_name: subdir in s3 where the data will be written. By default
		`table_name` will be used
		:table_name type: string

		:returns: None
		:rtype: None


		:example:

		>>> data2write = pd.DataFrame({'col1':[1,2,3,4]})
		>>> datawriter.write_parquet_data(data2write,data_path)
		"""
		
		if dir_name is None:
			dir_name = table_name

		for dateindex, df in df.groupby([df.index.date]):
			datestr = dateindex.strftime("%Y-%m-%d")
			# try:
			# 	df.drop(['timestamp'],axis=1,inplace = True)
			# except KeyError as ke:
			# 	pass

			#pdb.set_trace()

			path = "{b}/analytics_output/{dir_name}/{partition_name}".format(
				b=self.s3_bucket_name,
				dir_name=dir_name,
				partition_name="date={d}".format(d=datestr)
			)

			# pyarrow parquet needs a timestamp column
			#TODO: be careful of ns vs ms unit on timestamp conversion
			df['timestamp'] = pd.to_datetime(df['timestamp'])
			table = pa.Table.from_pandas(df, schema=output_table_schema.PYARROW_SCHEMA_TS)

			try:
				pq.write_to_dataset(table, path, filesystem=self.s3fs, compression='gzip')
			except Exception as e:
				logger.error(e)

			qs='ALTER TABLE `' + self.s3_bucket_name + '`.`' + table_name + '` ' +'ADD IF NOT EXISTS PARTITION (date=\'' + datestr + '\');'

			# response = client.start_query_execution(
			# 	QueryString='ALTER TABLE `' + self.s3_bucket_name + '`.`' + table + '` ' +
			# 				'ADD IF NOT EXISTS PARTITION (date=\'' + datestr + '\');',
			# 	ResultConfiguration={
			# 		'OutputLocation': "s3://{s3bucketname}/tmp".format(
			# 			s3bucketname=self.s3_bucket_name)
			# 	}
			# )
			# s3_output_dest = "s3://{s3bucketname}/tmp".format(
			# 			s3bucketname=s3_bucket_name)
			dqe = data_retrieval.DataQueryEngine(s3_bucket_name=self.s3_bucket_name,
												 boto_session=self._boto_session)
			try:
				dqe.execute_generic_query(qs, pull_result=False)
			except ClientError as e:
				logger.error(e)



	def write_parquet_on_off_data(self,df):
		"""
		write parquet data to s3 file system from pandas dataframe

		:param df: input data to be written to s3
		:df type: pandas DataFrame

		:returns: None
		:rtype: None


		:example:
		>>> data2write = pd.DataFrame({'col1':[1,2,3,4]})
		>>> datawriter.write_parquet_data(data2write)
		"""

		for dateindex, df in df.groupby([df.index.date]):
			datestr = dateindex.strftime("%Y-%m-%d")
			# try:
			# 	df.drop(['timestamp'],axis=1,inplace = True)
			# except KeyError as ke:
			# 	pass

			path = "{b}/analytics_output/on_off/{foldername}".format(
				b=self.s3_bucket_name,
				foldername="date={d}".format(d = datestr)
			)
			# df.index.names = ['index']
			#TODO: be careful of ns vs ms unit on timestamp conversion
			df['timestamp'] = pd.to_datetime(df['timestamp'])

			table = pa.Table.from_pandas(df, schema=output_table_schema.PYARROW_SCHEMA_TS)
			try:
				pq.write_to_dataset(table, path, filesystem=self.s3fs,compression='gzip')
			except Exception as e:
				logger.error(e)

			# add partition if table partition does not exist

			table_name = "on_off"
			qs='ALTER TABLE `' + self.s3_bucket_name + '`.`' + table_name + '` ' +'ADD IF NOT EXISTS PARTITION (date=\'' + datestr + '\');'
			dqe = data_retrieval.DataQueryEngine(s3_bucket_name=self.s3_bucket_name)
			try:
				dqe.execute_generic_query(qs)
			except ClientError as e:
				logger.error(e)


	def write_location_data(self,sensor,col,value,gateway_name):
		"""
		Write to or update data within a specific location table file.
		TODO: refactor to allow for multiple column and value updates

		:param sensor: the name of the sensor to update
		:type sensor: string

		:param col: the column to update
		:col type: string

		:param value: the value to update the column with
		:value type: string

		:param gateway_name: name of gateway is the s3 directory of the location file to write to
		:placement_path type: string
		"""

		super(DataWriter,self).write_location_data(sensor,col,value,gateway_name)


