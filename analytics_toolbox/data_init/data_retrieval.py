#!/usr/bin/env python3
"""
Module for handling data prior to consumption in analytics.
Retrieval, formatting, and massaging data until it is in the most manageable format
possible.
..author: Oluwatosin Sonuyi <oluwatosinsonuyi@mhsinc.net>
"""

import logging
import copy
from datetime import datetime, timezone, timedelta
import time
import csv
import s3fs

from botocore.exceptions import ClientError

import pandas as pd
import numpy as np

import boto3

import pdb

from analytics_sdk_core.constants import table_tags
from analytics_toolbox.data_init import data_handles
from analytics_sdk_core.units_manager import Q_
from analytics_sdk_core.data_init import data_pull

logger = logging.getLogger('analytics_toolbox.{}'.format(__name__))


class DataQueryEngine(data_pull.DataQueryEngineLite):
    """
    General purpose data pulling from Athena or direct S3 file reads
    with capabilities for retrying failed queries and error handling. Abstracts a few
    commonly used queries such as getting timeseries data and
    the sensor information. Also allows for querying tags across
    multiple tables without having to know where each tag is stored.
    """

    def __init__(
            self,
            s3_bucket_name=None,
            max_retries=2,
            throttle_delay_seconds=0.25,
            retry_sleep_time='5 seconds',
            boto_session=None
    ):
        """
        Need to have credentials stored in ~/.aws/credentials
        and region stored in ~/.aws/config
        Otherwise a boto3 session can be passed in as
        an optional parameter
        """

        super().__init__(s3_bucket_name,
                         max_retries,
                         throttle_delay_seconds,
                         retry_sleep_time,
                         boto_session)

    def _construct_ts_query_string(self, conveyor, equip, taglist, timerange_tuple):
        """
        input is a list of tags to query and datetimes
        and conveyor and equipment
        returns a dictionary of queries,
        the key is the table name to execute the query on
        and the value is the query string to execute

        :param conveyor: the name of the conveyor for data you are looking for
        :conveyor type: string

        :param equip: the equipment type, 'B' for bearings, 'G' for gearbox 'M' for motor
        :equip type: string

        :param taglist: list of tags that you hope to pull
        :taglist type: list of strings

        :param timerange_tuple: two tuples that indicate timerange for data of interest
        :timerange_tuple type: 2-tuple of datetime objects


        :returns: dictionary of queries, keys are the table to execute query on, value is the raw query string
        :rtype: (dictionary,Pandas DataFrame)
        """

        df_sensors = self.get_conveyor_sensors(conveyor)

        df_sensors_equip = df_sensors[df_sensors['equipment'] == equip.upper()]
        """
		TODO: should all data be combined into one timestamp that contains data for many assets in conveyor?
		just with the renamed columns such as gearbox_rms_vibration_x, motor_rms_vibration_x, bearing_rms_vibration_x
		current implementation has duplicated timestamps
		"""
        if equip.lower() == 'all':
            df_sensors_equip = df_sensors

        if df_sensors_equip.empty:
            logger.error('There was no equipment code matching {e} for {c}\nShutting down query engine'.format(
                e=equip, c=conveyor))
            self.quit_querying = True
            return {'': ''}

        t1 = min(timerange_tuple).strftime("%Y-%m-%d %H:%M:00")
        t2 = max(timerange_tuple).strftime("%Y-%m-%d %H:%M:00")
        queries = {}
        local_table_tags = {}
        # loop through taglist and see which table has each tag
        for tag in taglist:
            try:
                table_containing_tag = table_tags.TAG_LOCATIONS[tag]
            except KeyError:
                raise KeyError('{t} is not in schema'.format(t=tag))

            if table_containing_tag in local_table_tags.keys():
                local_table_tags[table_containing_tag].append(tag)
            else:
                local_table_tags[table_containing_tag] = [tag]

            if table_containing_tag.lower() == 'plc':
                df_sensors_equip = df_sensors_equip.append(df_sensors[df_sensors['equipment'] == 'PLC'])

        # create one query for each table
        for tname, taglist in local_table_tags.items():
            # add sensor column and equipment type to output
            taglist.append('sensor')
            if 'timestamp' not in taglist:
                taglist.append('timestamp')
            tag_str = ",".join(taglist)

            joined_sensor_filter = " or ".join(["sensor='{}'".format(sens)
                                                for sens in set(df_sensors_equip['sensor'].values) if not sens == 'nan'])

            is_partitioned = self.is_table_partitioned(table_name=tname)
            if is_partitioned:
                logger.debug("{} is partitioned, using date partition for query.".format(tname))
                # query with partitioning to prevent scanning full table
                d1 = min(timerange_tuple).strftime("%Y-%m-%d")
                d2 = max(timerange_tuple).strftime("%Y-%m-%d")
                query_string = """SELECT {comma_sep_tags} FROM {table}
				WHERE date between date '{d1}' AND date '{d2}'
				AND timestamp
				between timestamp '{t1}' AND timestamp '{t2}'
				AND ({sensor_eq});""".format(
                    comma_sep_tags=tag_str,
                    table=tname,
                    d1=d1,
                    d2=d2,
                    t1=t1,
                    t2=t2,
                    sensor_eq=joined_sensor_filter
                )

            else:
                query_string = """SELECT {comma_sep_tags} FROM {table} WHERE timestamp
				between timestamp '{t1}' AND timestamp '{t2}' AND ({sensor_eq});""".format(
                    comma_sep_tags=tag_str,
                    table=tname,
                    t1=t1,
                    t2=t2,
                    sensor_eq=joined_sensor_filter
                )
            if joined_sensor_filter == "":
                query_string = query_string.split('AND ()')[0]
            # TODO: restructure as nested query instead of returning the df_sensors_equip dataframe
            queries[tname] = query_string
        return queries, df_sensors_equip

    def _split_up_dfs_by_sensor(self, sensors_info_df, data_from_queries):
        """
        Split up the dataframe by sensors type

        :param sensors_info_df: the result of a query to the location table
        :type sensors_info_df: dict

        :param data_from_queries: all of the data returned from a query for all the sensors in the conveyor
        :type data_from_queries: pandas.DataFrame

        :returns: dictionary where the keys are equipment types and the values are a pandas DataFrame
        :rtype: dict
        """
        # only make sense to add equipment codes if query returns multiple equipment types
        # make sure sensor is string in the sensors_info_df
        sensors_info_df['sensor'] = sensors_info_df['sensor'].astype(str)
        # quick way to add data from location table to data dataframe
        sensor_map_dict = sensors_info_df.to_dict('list')
        # creates dictionaries that map sensor to conveyor, sensor to equipment, and sensor to detailed equipment type
        sensor2conveyor = {k: v for k, v in zip(sensor_map_dict['sensor'], sensor_map_dict['conveyor'])}
        # sensor2equipment = {k:v for k,v in zip(sensor_map_dict['sensor'],sensor_map_dict['equipment'])}
        sensor2detailed_equipment = {k: v for k, v in zip(sensor_map_dict['sensor'], sensor_map_dict['location'])}
        split_up_dfs_by_sensor = {}
        split_out_dfs = []
        for data_result in data_from_queries:
            # make sure sensors are strings and not integers
            data_result['sensor'] = data_result['sensor'].astype(str)
            # create new columns mapping sensors to their equipment types and conveyors
            data_result['equipment_type'] = data_result['sensor'].map(sensor2detailed_equipment)
            data_result['conveyor'] = data_result['sensor'].map(sensor2conveyor)
            # data_result['simple_equipment'] = data_result['sensor'].map(sensor2equipment)
            for sensor_name in data_result['equipment_type'].unique():
                sname = str(sensor_name)
                split_out_df = data_result[data_result['equipment_type'] == sname]
                # set datetime as index for each split out dataframe (must be before prepending equipment type name)
                split_out_df = data_handles.set_datetime_index(split_out_df)
                # prepend equipment type to column name except for generic columns
                cols2ignore = ['sensor', 'equipment_type', 'conveyor', 'simple_equipment']
                new_col_names = []
                for col in split_out_df.columns:
                    newname = '{}.{}'.format(sname, col) if col not in cols2ignore else col
                    new_col_names.append(newname)

                split_out_df.columns = new_col_names

                split_up_dfs_by_sensor[sname] = split_out_df
                split_out_dfs.append(split_out_df)

        logger.info('Returning {num} dataframes with following number of entries: \n{dfsizes}'.format(
            num=len(split_out_dfs),
            dfsizes={k: len(v) for k, v in split_up_dfs_by_sensor.items()})
        )

        return (split_up_dfs_by_sensor)

    def execute_timeseries_data_query(self, taglist, conveyor, equip, timerange_tuple, return_as_df=True, s3_bucket_name=None, cache_expiration=None):
        """
        Takes a list of tags, a conveyor, and the equipment of interest
        and returns a dataframe with the data
        for the specified timerange

        if the equip type is all, the return type will be a dictionary, not a dataframe

        Returns a string of data lines or a pandas dataframe
        """

        if not s3_bucket_name:
            s3_bucket_name = self.s3_bucket_name

        data_query_dict, sensors_info_df = self._construct_ts_query_string(conveyor, equip, taglist, timerange_tuple)
        if self.quit_querying:
            return_val = pd.DataFrame() if return_as_df else ''
            return return_val
        data_from_queries = []
        for tname, query_str in data_query_dict.items():

            data_from_query = self.execute_generic_query(
                raw_query_string=query_str,
                s3_bucket_name=s3_bucket_name,
                cache_expiration=cache_expiration,
                return_as_df=return_as_df)

            data_from_queries.append(data_from_query)
        if return_as_df:
            if equip.lower() == 'all':
                split_up_dfs_by_sensor = self._split_up_dfs_by_sensor(sensors_info_df, data_from_queries)
                return split_up_dfs_by_sensor
            else:
                # merge the data from plc and sensor tables together
                # set index as timestamp for dataframes so merge can occur
                data_from_queries = [data_handles.set_datetime_index(
                    df, drop_datetimes=True) for df in data_from_queries]
                # TODO: determine if this is a merge toleance that makes sense and maybe parameterize
                data = data_handles.merge_disparate_dfs(data_from_queries, merge_tol='1 second')

            logger.info('Columns in dataframe after adding equipment information: {cols}'.format(cols=data.columns))

        else:
            # if not returning as a df but still want to merge multiple data soruces
            if len(data_from_queries) > 1:
                raise NotImplementedError('No implementation for merging\
					data from more than one table as a string. Try calling\
					function with return_as_df parameter set to True')
            else:
                data = data_from_queries[0]
        return data

    def execute_location_query(self, sensor_name, gateway_name, s3_bucket_name=None, cache_expiration=None):
        """
        execute location query for a particular sensor
        returns dataframe for all fields within the location table for a particular sensor

        :param sensor_name: name of sensor to query location table for
        :sensor_name type: string of sensor name

        :returns: DataFrame withe one column for each entry
        :rtype: pandas DataFrame
        """

        location_lines = super().execute_location_query(sensor_name=sensor_name, gateway_name=gateway_name,
                                                        s3_bucket_name=s3_bucket_name, cache_expiration=cache_expiration)

        location_df = self._format_query_results_as_df(location_lines)

        return location_df

    def execute_stats_table_query(self, sensor_name, gateway_name, s3_bucket_name=None, cache_expiration=None):
        """
        execute stats table query for a particular sensor
        returns dataframe for all fields within the stats table for a particular sensor
        TODO: can this be easier to do without athena and just pull directly from S3

        :param sensor_name: name of sensor to query stats table for
        :sensor_name type: string of sensor name

        :returns: DataFrame withe one column for each entry
        :rtype: pandas DataFrame
        """

        stats_lines = super().execute_stats_table_query(sensor_name=sensor_name, gateway_name=gateway_name,
                                                        s3_bucket_name=s3_bucket_name, cache_expiration=cache_expiration)

        stats_df = self._format_query_results_as_df(stats_lines)

        return stats_df

    def execute_generic_query(self, raw_query_string, return_as_df=False, s3_bucket_name=None, cache_expiration=None, pull_result=True):
        """
        Query Data from Athena

        :param raw_query_string: the raw sql query to execute with the engine
        :raw_query_string type: string

        :param return_as_df: boolean value indicating if the results of
        the query should be returned as a dataframe
        :return_as_df type: boolean

        :param s3_bucket_name: name of s3 bucket, only needed if AthenaQueryEngine was not initialized with one
        :s3_bucket_name type: string

        :returns: data that was requested based on the query string

        """

        query_result_lines = super().execute_generic_query(
            raw_query_string=raw_query_string,
            s3_bucket_name=s3_bucket_name,
            cache_expiration=cache_expiration,
            pull_result=pull_result
        )

        if return_as_df:
            query_result = self._format_query_results_as_df(query_result_lines)
        else:
            query_result = query_result_lines

        return query_result

    def _format_query_results_as_df(self, result_lines):
        """
        Take result lines from athena query and
        turn them into a pandas dataframe with numeric
        column datatypes where applicable
        """

        if len(result_lines) > 0:
            # make a dataframe
            df = pd.DataFrame(result_lines)

            # convert to numeric datatypes where applicable, otherwise ignore
            df = df.apply(pd.to_numeric, errors='ignore')
            return df

        else:
            logger.warning('Query returned an empty result')
            return pd.DataFrame()

    def get_conveyors(self, s3_bucket_name=None, cache_expiration=None):
        """
        execute query for getting all the conveyors that exist for a particular site

        :param cache_expiration: amount of time the cache should exist for

        :returns: list of conveyors available at one site
        :rtype: list
        """

        conveyors_list = super().get_conveyors(s3_bucket_name=s3_bucket_name, cache_expiration=cache_expiration)

        return conveyors_list

    def get_conveyor_sensors(self, conveyor, cache_expiration=None):
        """
        get all of the sensors and information for sensors of a particular conveyor
        TODO: change to return all of the sensor columns

        :param conveyor: string of conveyor name to get sensors information for
        :conveyor type: string

        :returns: pandas dataframe of location fields for all the sensors of a particular conveyor
        :rtype: pandas DataFrame
        """

        sensor_dict = super().get_conveyor_sensors(conveyor=conveyor, cache_expiration=cache_expiration)

        sensor_df = self._format_query_results_as_df(sensor_dict)

        return sensor_df


def construct_relative_timestamp_tuple(time_back, tzoffset=0):
    """
    Construct a tuple of timestamps with one entry being the current time
    and the other one being the currenttime minus the time back entered

    time_back can be entered as a string with units attached
    '12 hours'
    '43 weeks'
    '17 seconds'
    '1.45 days'

    the unist_manager module will convert and calculate the appropriate timestamps
    """
    # use unit manager to convert units to days before passing into timedelta

    return data_pull.construct_relative_timestamp_tuple(time_back, tzoffset)
