#!/usr/bin/env python3
"""
Module for handling data prior to consumption in analytics.
Retrieval, formatting, and massaging data until it is in the most manageable format
possible.
..author: Oluwatosin Sonuyi <oluwatosinsonuyi@mhsinc.net>
"""

import logging
import copy
import pandas as pd
import numpy as np

import boto3

from analytics_sdk_core import constants

from analytics_sdk_core.units_manager import Q_

logger = logging.getLogger('analytics_toolbox.{}'.format(__name__))


def set_datetime_index(df, drop_datetimes=True):
    """
    Create datetime as index
    """
    dt_col = set(df.columns) & set(constants.TIMESTAMP_COLS)
    if len(dt_col) < 1:
        logger.info('No datetime column found in {}, hopefully already an index'.format(df.columns))
        return df

    elif len(dt_col) > 1:
        dt_col = set([dt_col.pop()])
        logger.warning('found multiple datetime columns, using: {}'.format(dt_col))
    try:
        dt_col = dt_col.pop()
        df.index = pd.DatetimeIndex(df[dt_col])
    # TODO: should we raise this error?
    except ValueError as ve:
        logger.error('incompatible datetime string format: {}'.format(ve))
        return df

    if drop_datetimes:
        for tstamp in constants.TIMESTAMP_COLS:
            if tstamp in df.columns:
                df = df.drop(columns=[tstamp])
    return df


def merge_disparate_dfs(df_list, merge_tol='16s', merge_method='nearest'):
    """
    create dataframes by merging disparate data sources i.e. plc and sensor data. 
    timestamps will not match exactly so this function will 
    match rows to the nearest timestamp within a specified interval i.e. 1 minute.
    this is why we need a data pull & interpolation service

    :param df_list: list of dataframes with data to merge 
    :df_list type: List [pandas Dataframe]

    :param merge_tol: (optional) the maximum time difference allowable for merging
    :merge_tol type: string

    :param merge_method: (optional) how to do the merge, "nearest" or "round"
    :merge_method type: string

    :returns: one dataframe with all the data merged together
    :rtype: pandas Dataframe
    """

    """
	Ensure type safety of expected inputs
	TODO: Check for index of timestamp type in each df
	"""
    try:

        # create inline function to check types of input
        check = lambda d: isinstance(d, pd.DataFrame)

        boolean_dfs = [check(df) for df in df_list]

        # ensure type safety, all must be dataframes
        if not all(boolean_dfs):
            raise TypeError
        # make sure there is at least one dataframe to merge
        elif len(df_list) < 1:
            raise IndexError
        else:
            # initialize merging with the firt dataframe available
            init_df = df_list[0]
            if len(df_list) == 1:
                return init_df

    except IndexError:
        logger.error('df_list must have at least 1 entries.')
        raise IndexError('df_list must have at least 1 entries.')
    except TypeError:
        logger.error('df_list must contain dataframes')
        raise TypeError('df_list must contain dataframes')

    # make each index a timestamp before merging
    df_list = [set_datetime_index(tempdf) for tempdf in df_list]

    """
	logic for continuous merging of dataframes
	skip over the first dataframe
	"""
    """
	Instead of doing merge_asof we can do rounding to nearest
	merge_tol second and then do an outer join, then delete duplicates.
	This should minimize data loss and maximize aggregation of close timestamps.

	merge_asof does a left join
	"""
    if merge_method == "round":
        init_df.index = init_df.index.round(merge_tol)
    init_df = init_df[~init_df.index.duplicated(keep='first')]
    for df in df_list[1:]:
        try:
            df = df[~df.index.duplicated(keep='first')]
            if merge_method.lower() == 'round':
                df.index = df.index.round(merge_tol)
                # lsuffix, rsuffix are excluded because we want all the dfs to have different columns
                init_df = init_df.join(df, how='outer')
            elif merge_method.lower() == 'nearest':
                init_df = pd.merge_asof(left=init_df.dropna(),
                                        right=df.dropna(),
                                        right_index=True,
                                        left_index=True,
                                        direction='nearest',
                                        tolerance=pd.Timedelta(merge_tol))
        except ValueError:
            # catch unsorted index error or no suffix specification error
            init_df = init_df.sort_index(inplace=False)
            df = df.sort_index(inplace=False)
            if merge_method.lower() == 'round':
                init_df = init_df.join(df, how='outer', rsuffix='_x', lsuffix='_y')
            elif merge_method.lower() == 'nearest':
                init_df = pd.merge_asof(left=init_df.dropna(),
                                        right=df.dropna(),
                                        right_index=True,
                                        left_index=True,
                                        direction='nearest',
                                        tolerance=pd.Timedelta(merge_tol))

    init_df = init_df[~init_df.index.duplicated(keep='first')]
    return_df = init_df.sort_index(inplace=False)

    return return_df
