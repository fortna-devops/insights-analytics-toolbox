#!/usr/bin/env python3
"""
Preparing data for ingestion into machine learning algorithms.
Don't be surprised to see mostly wrappers around scikit functions.

..author: Oluwatosin Sonuyi <oluwatosinsonuyi@mhsinc.net>
"""

import logging

import pandas as pd
import numpy as np

from sklearn.preprocessing import MinMaxScaler
from sklearn.preprocessing import Imputer



logger = logging.getLogger('analytics_toolbox.{}'.format(__name__))



def split_train_test(input_df,train_ratio=2./3.,shuffle=False):
	"""
	split input dataframe into a train set and a test set
	wrapper around scikit functionality

	:param input_df: dataframe to be split up
	:input_df type: pandas Dataframe

	:param train_ratio: (optional) decimal percentage of data to use in training set
	:train_ratio type: float

	:param shuffle: (optional) should the data be shuffled before train/test


	:returns: two dataframes, one train and one test
	:rtype: pandas Dataframe
	"""

	train_num = int(train_ratio*len(input_df))
	if shuffle:
		raise NotImplementedError('Shuffle implementation not built yet')
	train_set = input_df[:train_num]
	test_set = input_df[train_num:]


	return train_set, test_set



def separate_target(input_df,target_colnames,drop_low_corr=False,corr_method='spearman',corr_threshold=0.001):
	"""
	Function to separate targets within input data frame from the features.

	:param input_df: full dataset to be separated
	:type input_df: pandas DataFrame

	:param target_colnames: column name of the target variable(s) to be predicted
	:type target_colnames: list

	# TODO should this be drop HIGHLY correlated values instead of comparing to target compare to eachother
	:param drop_low_corr: whether or not to drop columns that are not correlated with target columns
	:type drop_low_corr: boolean

	:param corr_method: correlation method to determine columns to drop (pearson, spearman, kendall)
	:type corr_method: string

	:param corr_threshold: absolute value cutoff for determining low correlation
	:type corr_threshold: float

	:returns: two dataframes, one for features and one for targets
	:rtype: pandas DataFrames
	"""

	cols2drop = target_colnames
	if drop_low_corr:
		raise NotImplementedError('Dropping low correlation columns is not yet implemented')

	try:
		features = input_df.drop(cols2drop,axis=1)
	except KeyError:
		logger.warn('Target column {} was not within input dataframe columns: {}'.format(target_colnames,input_df.columns))
	
	target = input_df[target_colnames]


	return features, target

def scale_data(input_df,feature_range=(0,1)):
	"""
	Function to scale data to common scale before input into machine learning models.

	:param input_df: full data set to be scaled
	:type input_df: pandas DataFrame

	:param feature_range: min and max value to scale to
	:type feature_range: tuple

	:returns: scaled dataframe with column names intact and MinMaxScaler
	:rtype: Tuple (pandas DataFrame, MinMaxScaler)
	"""
	scaler = MinMaxScaler(feature_range=feature_range,copy=True)
	try:
		df_scaled = scaler.fit_transform(input_df)
	except ValueError as ve:
		logger.warning('{}\n dropping nans'.format(ve))
		nonans_df = input_df.dropna(inplace=False)
		df_scaled = scaler.fit_transform(nonans_df)

	# put column names back
	df_scaled = pd.DataFrame(df_scaled,columns=input_df.columns)

	return df_scaled, scaler

def inverse_scale_data(df_to_transform, **kargs):
	"""
	scales output data

	:param df_to_transform: Dataframe with scaled values
	:type df_to_transform: pandas DataFrame

	:param minmaxscaler: MinMaxScaler used to transform the input data
	only required if original_df is not provided. (this only works if the data used
	to train the minmaxscaler is of the same form/dimensions as the df to inverse transform)
	:type minmaxscaler: MinMaxScaler 

	:param original_df: the original dataframe used to transform the input data
	only required if minmaxscaler is not provided (in progress)
	:type original_df: pandas DataFrame

	:returns: dataframe with values transformed back to original scale
	:rtype: pandas DataFrame
	"""

	keyword_arg_set = kargs.keys() & {'minmaxscaler','original_df'}

	if len(keyword_arg_set) != 1:
		raise ValueError('One keyword argument is required, either minmaxscaler=x or original_df=x but not both.')

	keyword_arg = keyword_arg_set.pop()

	if keyword_arg == 'minmaxscaler':
		
		if isinstance(kargs[keyword_arg],MinMaxScaler):
			mms = kargs[keyword_arg]
		else:
			raise TypeError('minmaxscaler parameter must be a MinMaxScaler type')

	elif keyword_arg == 'original_df':
		# if its a pandas dataframe will need to grab first max
		# if its a numpy array then the max will be taken across the whole m x n structure
		# original_df = kargs[keyword_arg]
		# if isinstance(original_df,pd.DataFrame):
		# 	mm_min = original_df.min()[0]
		# 	mm_max = original_df.max()[0]
		# elif isinstance(original_df,np.ndarray):
		# 	mm_min = original_df.min()
		# 	mm_max = original_df.max()
		# else:
		# 	raise TypeError('original_df parameter must be a pandas dataframe or numpy nd array')
		# logger.debug('min: {} max: {}'.format(mm_min,mm_max))
		mms = MinMaxScaler()
		try:
			mms.fit(kargs[keyword_arg])
		except ValueError:
			mms.fit(kargs[keyword_arg].values.reshape(-1,1))

	logger.debug('df to transform: {df_shape}'.format(df_shape=df_to_transform))
	try:
		new_df = mms.inverse_transform(df_to_transform)
	except ValueError:
		input_shape = df_to_transform.shape
		# if its a Series, we need the values from it before calling reshape
		try:
			new_df_to_transform = df_to_transform.reshape(-1,1)
		except AttributeError:
			new_df_to_transform = df_to_transform.values.reshape(-1,1)
		logger.info('Reshaping df to transform from 1D array to 2D array\n From: {input}\n\
			to: {new_input}'.format(input=input_shape,
				new_input=new_df_to_transform.shape))

		new_df = mms.inverse_transform(new_df_to_transform)

	logger.debug('Transformed DF: {}'.format(new_df))

	# add columns back to dataframe after inverse scaling
	try:
		df_scaled = pd.DataFrame(new_df,columns=df_to_transform.columns)
	except AttributeError:
		logger.warning('Passed in a numpy ndarray instead of a pandas dataframe, can not re add column names, returning a dataframe with numerical column names')
		df_scaled = pd.DataFrame(new_df)


	return df_scaled



def drop_no_variance(input_df,var_limit,replacement_val=np.nan):
	"""
	Replaces columns with low variance with
	:param input_df: dataframe of input data
	:input_df type: pandas DataFrame

	:param var_limit: the float limit of variance for the flatline 
	:var_limit type: float

	:param replacement_val: the replacement values for columns with low variance
	:replacement_val type: float

	:returns: dataframe with non varying columns replaced with replacement value
	:rtype: pandas Dataframe
	"""

	return pd.DataFrame()


def drop_high_correlations(input_df,corr_limit):
	"""
	Replaces columns with low variance with
	:param input_df: dataframe of input data
	:input_df type: pandas DataFrame

	:param var_limit: the float limit of variance for the flatline 
	:var_limit type: float

	:param replacement_val: the replacement values for columns with low variance
	:replacement_val type: float

	:returns: dataframe with non varying columns replaced with replacement value
	:rtype: pandas Dataframe
	"""


	# Create correlation matrix
	corr_matrix = input_df.corr().abs()

	# Select upper triangle of correlation matrix
	upper = corr_matrix.where(np.triu(np.ones(corr_matrix.shape), k=1).astype(np.bool))

	# Find index of feature columns with correlation greater than 0.95
	to_drop = [column for column in upper.columns if any(upper[column] > corr_limit)]

	output_df = input_df.drop(columns=to_drop)

	return output_df




def impute_data(input_df):
	imp = Imputer(missing_values=np.nan,strategy='median',axis=0,verbose=0,copy=True)

	new_df = imp.fit_transofrm(input_df)

	return pd.DataFrame(new_df,columns=input_df.columns)









