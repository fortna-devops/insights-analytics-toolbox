#!/usr/bin/env python3
"""
Machine learning algorithms Fitting & Predictions
Don't be surprised to see mostly wrappers around scikit functions.

..author: Oluwatosin Sonuyi <oluwatosinsonuyi@mhsinc.net>
"""

import logging
from abc import ABC, abstractmethod

import pandas as pd
import numpy as np

from sklearn.svm import SVR
from sklearn.neural_network import MLPRegressor
from sklearn.ensemble import GradientBoostingRegressor


from analytics_toolbox.machine_learning import data_preparation



logger = logging.getLogger('analytics_toolbox.{}'.format(__name__))


class ModelTrainer():

	def __init__(self, dataframe, target, algorithm='MLP', train_ratio=3/5.):

		self.scaling_required = True
		self.actuals = dataframe
		self.target = target
		self.algorithm = algorithm
		self.train_ratio = train_ratio

		self._initial_fit_predict()

	@property
	def residuals(self):
		return self._residuals

	@residuals.setter
	def residuals(self,resids):
		self._residuals = resids

	@property
	def estimates(self):
		return self._estimates

	@estimates.setter
	def estimates(self,estims):
		self._estimates = estims

	@property
	def actuals(self):
		return self._actuals

	@actuals.setter
	def actuals(self, acts):
		self._actuals = acts

	@property
	def model_object(self):
		return self._model_object

	@model_object.setter
	def model_object(self,model_obj):
		self._model_object = model_obj
	

	def _initial_fit_predict(self):
		df = self.actuals
		df = df.dropna()
		if self.scaling_required:
			df, _ = data_preparation.scale_data(self.actuals)

		df_train,df_test = data_preparation.split_train_test(df,train_ratio=self.train_ratio)
		features_train,target_train = data_preparation.separate_target(df_train,target_colnames=[self.target])
		features_test,target_test = data_preparation.separate_target(df_test,target_colnames=[self.target])

		# TODO: replace this with an algorithm map outside of here maybe in the constants file (watch out for circular imports)

		ml_algo_map = {'MLP':MultilayerPerceptronFitPredict,
					   'SVRM':SupportVectorMachineFitPredict,
					   'GTBoost':GTBoostingFitPredict}

		if self.algorithm == 'MLP':
			est,res, model = MultilayerPerceptronFitPredict(features_train,target_train,features_test,target_test,return_model=True)
		elif self.algorithm == 'SVMR':
			est,res, model = SupportVectorMachineFitPredict(features_train,target_train,features_test,target_test,return_model=True)
		elif self.algorithm == 'GTBoost':
			est, res, model = GTBoostingFitPredict(features_train,target_train,features_test,target_test,return_model=True)
		else:
			logger.error('Algorithm: {algo} is not defined in this package.'.format(algo=self.algorithm))


		scaled_back_truth = data_preparation.inverse_scale_data(target_test[self.target],original_df=self.actuals[self.target])
		scaled_back_est = data_preparation.inverse_scale_data(est,original_df=self.actuals[self.target])

		self.residuals = scaled_back_truth - scaled_back_est
		self.estimates = scaled_back_est

		self.model_object = model

	def predict(self, features_input_df):


		if self.scaling_required:
			df = data_preparation.scale_data(features_input_df)
		else:
			df = features_input_df

		estimates = self.model_obj.predict(df)




def GTBoostingFitPredict(features_train,
						  target_train,
						  features_test,
						  target_test,
						  return_model=False):
	"""
	Gradient Boosting Tree Regression
	"""
	n_estimators = 100
	learning_rate = 0.1
	max_depth = 1
	random_state = 0
	loss = 'ls'


	gtb_model = GradientBoostingRegressor(n_estimators=100, 
										  learning_rate=0.1,
										  max_depth=1,
										  random_state=0,
										  loss='ls')
	gtb_model.fit(features_train.values, target_train.values.ravel())

	estimates = gtb_model.predict(features_test)

	residuals = estimates - target_test.values.ravel()

	if return_model:
		return estimates, residuals, gtb_model

	return estimates, residuals




def SupportVectorMachineFitPredict(features_train,
									target_train,
									features_test,
									target_test,
									return_model=False):
	"""
	support vector machine regression

	:param input_df: dataframe to be split up
	:input_df type: pandas Dataframe

	:param train_ratio: (optional) decimal percentage of data to use in training set
	:train_ratio type: float

	:param shuffle: (optional) should the data be shuffled before train/test


	:returns: two dataframes, one train and one test
	:rtype: pandas Dataframe

	TODO: use kwargs for different params in ML
	"""

	# lets use kernel radial based function bc nonlinearity
	# other parameters are just using default. 
	c = 1.
	kernel = 'rbf'
	epsilon = 0.01
	degree = 3

	svr = SVR(C=c,kernel=kernel,epsilon=epsilon,degree=degree)
	# ravel to get rid of warning on column vector vs 1d array
	logger.debug('Fitting SVR with following parameters: {params}'.format(params=svr.get_params()))
	svr.fit(features_train.values,target_train.values.ravel())
	estimates = svr.predict(features_test)

	residuals = estimates - target_test.values.ravel()

	if return_model:
		return estimates, residuals, svr

	return estimates, residuals

def MultilayerPerceptronFitPredict(features_train,
									target_train,
									features_test,
									target_test,
									return_model=False):

	"""
	MLP Regression

	:param input_df: dataframe to be split up
	:input_df type: pandas Dataframe

	:param train_ratio: (optional) decimal percentage of data to use in training set
	:train_ratio type: float

	:param shuffle: (optional) should the data be shuffled before train/test


	:returns: two dataframes, one train and one test
	:rtype: pandas Dataframe

	TODO: use kwargs for different params in ML
	"""
	act = 'relu'
	mlpr = MLPRegressor(activation=act)
	logger.debug('Fitting MLPRegressor with following parameters: {params}'.format(params=mlpr.get_params()))
	mlpr.fit(features_train.values,target_train.values.ravel())
	estimates = mlpr.predict(features_test)

	residuals = estimates - target_test.values.ravel()

	if return_model:
		return estimates, residuals, mlpr

	return estimates, residuals



