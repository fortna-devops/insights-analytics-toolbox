#!/usr/bin/env python3

import logging
import copy
import pandas as pd
import numpy as np

logger = logging.getLogger('analytics_toolbox.{}'.format(__name__))

def range_check_df(input_df,range_dict,replacement_val=np.nan):
	"""
	:param input_df: dataframe of input data
	:input_df type: pandas Dataframe

	:param range_dict: dictionary specificy ranges of tags in input_df
	:range_dict type: Dictionary

	:param replacement_val: value to replace data that is out of range with
	:type replacement_val: Float

	:returns: dataframe with data out of range replaced with replacement_val
	:rtype: pandas Dataframe
	"""
	output_df = input_df.copy()
	cols = input_df.columns
	for signal,rangelist in range_dict.items():
		
		if signal in cols:
			if len(rangelist) != 2:
				raise ValueError('Need to enter in a range of values')
			try:
				rangelist = list(map(float,rangelist))
			except ValueError:
				raise ValueError('Could not cast values in {} to floats.'.format(rangelist))
			logger.debug('{} range: {}'.format(signal,rangelist))
			low_limit = min(rangelist)
			high_limit = max(rangelist)
			# indices of replacable data that is outside of a predefined range
			replace_idx = (output_df[signal]>high_limit)|(output_df[signal]<low_limit)
			# replace the values out of range with replacement values
			output_df.loc[replace_idx,signal] = replacement_val

	return output_df