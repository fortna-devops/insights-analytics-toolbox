from setuptools import setup, find_packages


with open('requirements.txt') as f:
    required = f.readlines()


setup(
   name='analytics_toolbox',
   version='0.0.1',
   description='A module full of reusable functions for analytics development.',
   author='MHS Insights Team',
   author_email='oluwatosinsonuyi@mhsinc.net',
   packages=find_packages(),
   install_requires=required
)
