#!/usr/bin/env python3

import pytest
import pandas as pd
import numpy as np
from analytics_toolbox.preprocessing import data_cleaning

@pytest.fixture
def input_df():
	df_dict_src0 = {
			   'tag10':[1,2,3,4,5],
			   'tag20':[6,7,8,9,10],
			   'tag30':[11,12,13,14,15]
			   }

	df0 = pd.DataFrame(df_dict_src0)


	return df0


def test_range_check(input_df):
	rd = {'tag10':[2,4],'tag30':[0,10]}
	df = data_cleaning.range_check_df(input_df,rd)
	assert np.isnan(df['tag10'].iloc[0])
	assert np.isnan(df['tag10'].iloc[-1])
	assert all(np.isnan(df['tag30']))

