#!/usr/bin/env python3
"""
Tests for the Data Writer class
Test for the non parquet writing functions are stored in sdk_core
..author: Oluwatosin Sonuyi <oluwatosinsonuyi@mhsinc.net>

TODO:
add test to check that other columns dont change
add tests for changing multiple columns at once once function is updated
"""

import json
import pytest
import csv

import pandas as pd
import numpy as np
import numpy.testing as npt

from moto import mock_s3
import boto3

import s3fs
import pyarrow.parquet as pq

import pyarrow.filesystem as pafs

from analytics_toolbox.data_init import data_writing
from analytics_sdk_core.analytic_exceptions import exceptions

import pdb


def check_equal(L1, L2):
    L1 = list(L1)
    L2 = list(L2)
    return (len(L1) == len(L2)) and (sorted(L1) == sorted(L2))


@mock_s3
@pytest.mark.skip(reason="requests are blocked")
def test_data_write_ts_parquet():
    """
    test for writing parquet files
    """

    bucket_name = 'mhspredict-testing'
    # create s3 bucket with mock location file in it
    conn = boto3.resource('s3', region_name='us-east-1')

    conn.create_bucket(Bucket=bucket_name)
    s3_client = boto3.client('s3')
    df = pd.read_csv('tests/test_data_init/test_preds_df.csv')
    df.index = pd.DatetimeIndex(df['timestamp'])
    # testing data writer functionality

    # read data from prediction table and see if it has been appropriately updated
    query_full_path = "s3://{bucket}/analytics_output/timeseries_predictions/date=2018-10-16".format(
        bucket=bucket_name)
    query_full_path2 = "s3://{bucket}/analytics_output/timeseries_predictions/date=2018-10-17".format(
        bucket=bucket_name)

    fs = s3fs.S3FileSystem()

    data_writer = data_writing.DataWriter(s3_bucket_name=bucket_name)

    data_writer.write_parquet_ts_predictions_data(
        df=df
    )

    df_day1 = pq.ParquetDataset(query_full_path, filesystem=fs).read_pandas().to_pandas()
    df_day2 = pq.ParquetDataset(query_full_path2, filesystem=fs).read_pandas().to_pandas()

    result_df = df_day1.append(df_day2)

    # delete the files after they have been read, whether they are correct or not
    try:
        pafs.S3FSWrapper(fs).delete(query_full_path, recursive=True)
        pafs.S3FSWrapper(fs).delete(query_full_path2, recursive=True)
    except OSError as e:
        pass

    assert isinstance(result_df.index, pd.DatetimeIndex)
    assert check_equal(df.columns, result_df.columns)

    number_cols = ['rms_velocity_z', 'rms_velocity_z_residuals', 'rms_velocity_x',
                   'rms_velocity_x_residuals', 'temperature', 'temperature_residuals']
    npt.assert_almost_equal(result_df[number_cols].values, df[number_cols].values, decimal=3)


@mock_s3
@pytest.mark.skip(reason="requests are blocked")
def test_data_write_on_off_parquet():
    """
    test for writing parquet files
    """
    bucket_name = 'mhspredict-testing'

    # create s3 bucket with mock location file in it
    conn = boto3.resource('s3', region_name='us-east-1')

    conn.create_bucket(Bucket=bucket_name)
    s3_client = boto3.client('s3')

    df = pd.read_csv('tests/test_data_init/test_onoff_df.csv')
    df.index = pd.DatetimeIndex(df['timestamp'])

    # df.drop(columns=['timestamp'],inplace=True,axis=1)

    # testing data writer functionality
    data_writer = data_writing.DataWriter(s3_bucket_name=bucket_name)

    data_writer.write_parquet_on_off_data(
        df=df
    )

    query_full_path = "s3://{bucket}/analytics_output/on_off/date=2018-10-17".format(
        bucket=bucket_name)

    fs = s3fs.S3FileSystem()
    result_df = pq.ParquetDataset(query_full_path, filesystem=fs).read_pandas().to_pandas()

    # delete the files after they have been read, whether they are correct or not
    try:
        pafs.S3FSWrapper(fs).delete(query_full_path, recursive=True)
    except OSError as e:
        pass

    assert isinstance(result_df.index, pd.DatetimeIndex)
    assert check_equal(df.columns, result_df.columns)

    pafs.S3FSWrapper(fs).delete(query_full_path, recursive=True)
