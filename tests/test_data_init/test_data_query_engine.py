#!/usr/bin/env python3
"""
Tests for the Athena Query Engine
..author: Oluwatosin Sonuyi <oluwatosinsonuyi@mhsinc.net>
"""
from datetime import datetime
from dateutil import parser
import json
import pytest
import pandas as pd
import numpy as np
import boto3
from moto import mock_s3
import pdb
import pickle


from analytics_toolbox.data_init import data_retrieval
from analytics_sdk_core.units_manager import Q_


@pytest.fixture
def input_df():
    df_dict_src0 = {
        'tag10': [1, 2, 3, 4, 5],
        'tag20': [6, 7, 8, 9, 10],
        'tag30': [11, 12, 13, 14, 15]
    }
    df0 = pd.DataFrame(df_dict_src0)

    return df0
"""
TODO re implement tests with s3 bucket specifically data for testing
"""


@pytest.mark.skip(reason="athena queries cost money and are slow.")
def test_data_query_athena():
    S3BUCKET_NAME = 'mhspredict-site-fedex-louisville'

    aqe = data_retrieval.DataQueryEngine(s3_bucket_name=S3BUCKET_NAME)

    q = """SELECT "sensor","temp_score","thresholds" FROM \"{}\".location where sensor!= 'sensor' LIMIT 3""".format(
        S3BUCKET_NAME)

    df = aqe.execute_generic_query(
        raw_query_string=q,
        return_as_df=True
    )

    assert type(df) == type(pd.DataFrame())
    assert 'sensor' in df.columns
    assert 'temp_score' in df.columns
    assert 'thresholds' in df.columns
    assert len(df.columns) == 3
    assert np.dtype('int64') in set(df.dtypes) or np.dtype('float64') in set(df.dtypes)


@pytest.mark.skip(reason="athena queries cost money and are slow.")
def test_data_query_timeseries_sensors():
    """
    test query for timeseries data from sensor table
    """
    S3BUCKET_NAME = 'mhspredict-site-fedex-louisville'

    aqe = data_retrieval.DataQueryEngine(s3_bucket_name=S3BUCKET_NAME)

    tags = ['temperature', 'rms_velocity_x', 'kurtosis_z']

    t1 = datetime(day=16, month=8, year=2018, hour=10, minute=30)
    t2 = datetime(day=16, month=8, year=2018, hour=12, minute=33)
    timetuple = (t1, t2)
    df = aqe.execute_timeseries_data_query(taglist=tags,
                                           timerange_tuple=timetuple,
                                           conveyor='SS1-4',
                                           equip='G',
                                           return_as_df=True)

    # return type should be a dataframe
    assert type(df) == type(pd.DataFrame())
    # each tag should be a column in returned df
    for tag in tags:
        assert tag in df.columns
    # should be a column for each tag requested plus sensor
    assert (len(tags) + 1) == len(df.columns)
    # at least one of the columns should be type float64
    assert np.dtype('float64') in set(df.dtypes)
    # timestamps returned should be in range queried
    for entry in df.index.values:
        entry_dt = parser.parse(str(entry))
        assert entry_dt > t1
        assert entry_dt < t2


@pytest.mark.skip(reason="athena queries cost money and are slow.")
def test_data_query_timeseries_plc():
    """
    test query for timeseries data from plc table
    """
    S3BUCKET_NAME = 'mhspredict-site-dhl-miami'

    aqe = data_retrieval.DataQueryEngine(s3_bucket_name=S3BUCKET_NAME)

    tags = ['vfd_current', 'vfd_frequency', 'vfd_voltage']
    t1 = datetime(day=19, month=10, year=2018, hour=10, minute=30)
    t2 = datetime(day=20, month=10, year=2018, hour=12, minute=33)
    timetuple = (t1, t2)

    df = aqe.execute_timeseries_data_query(taglist=tags,
                                           timerange_tuple=timetuple,
                                           conveyor='T2-F1-1',
                                           equip='B',
                                           return_as_df=True)

    # return type should be a dataframe
    assert type(df) == type(pd.DataFrame())
    # each tag should be a column in returned df
    for tag in tags:
        assert tag in df.columns
    # should be a column for each tag requested plus one for sensor
    assert (len(tags) + 1) == len(df.columns)
    # at least one of the columns should be type float64
    assert np.dtype('float64') in set(df.dtypes)
    # timestamps returned should be in range queried
    for entry in df.index.values:
        entry_dt = parser.parse(str(entry))
        assert entry_dt > t1
        assert entry_dt < t2


@pytest.mark.skip(reason="athena queries cost money and are slow.")
def test_data_query_timeseries_sensors_and_plc():
    """
    Test the creation of a single dataframe when tags
    are pulled from different tables, i.e. sensor
    and plc tables
    """

    S3BUCKET_NAME = 'mhspredict-site-fedex-louisville'

    aqe = data_retrieval.DataQueryEngine(s3_bucket_name=S3BUCKET_NAME)

    tags = ['vfd_current', 'vfd_frequency', 'vfd_voltage',
            'temperature', 'rms_velocity_x', 'kurtosis_z']

    t1 = datetime(day=16, month=10, year=2018, hour=12, minute=30)
    t2 = datetime(day=16, month=10, year=2018, hour=12, minute=40)
    timetuple = (t1, t2)
    df = aqe.execute_timeseries_data_query(taglist=tags,
                                           equip='M',
                                           conveyor='SS1-4',
                                           timerange_tuple=timetuple,
                                           return_as_df=True)

    print(df)
    # return type should be a dataframe
    assert type(df) == type(pd.DataFrame())
    # each tag should be a column in returned df
    for tag in tags:
        assert tag in df.columns
    # should be a column for each tag requested
    assert len(tags) == len(df.columns)
    # at least one of the columns should be type float64
    assert np.dtype('float64') in set(df.dtypes)
    # timestamps (the index) returned should be in range queried
    for entry in df.index.values:
        entry_dt = parser.parse(str(entry))
        assert entry_dt > t1
        assert entry_dt < t2


@pytest.mark.skip(reason="athena queries cost money and are slow.")
def test_conveyor_all_equipment_query():
    """
    test grabbing all the data associated with a specific conveyor
    across multiple assets
    """
    S3BUCKET_NAME = 'mhspredict-site-fedex-louisville'

    aqe = data_retrieval.DataQueryEngine(s3_bucket_name=S3BUCKET_NAME)

    tags = ['vfd_current', 'vfd_frequency', 'vfd_voltage',
            'temperature', 'rms_velocity_x', 'kurtosis_z']

    t1 = datetime(day=10, month=8, year=2018, hour=9, minute=33)
    t2 = datetime(day=10, month=8, year=2018, hour=12, minute=33)
    timetuple = (t1, t2)
    dfs_dict = aqe.execute_timeseries_data_query(taglist=tags,
                                                 equip='all',
                                                 conveyor='SS1-4',
                                                 timerange_tuple=timetuple,
                                                 return_as_df=True)
    # return type should be a dictionary
    assert type(dfs_dict) == dict
    # should be a column for each tag requested
    # assert len(tags) == len(df.columns)
    # at least one of the columns should be type float64
    for n, df in dfs_dict.items():
        g = df.columns[0].split('.')[0]
        df.to_csv('{}_{}.csv'.format(g, n))
        assert np.dtype('float64') in set(df.dtypes)
        assert 'equipment_type' in df.columns
        assert type(df.index) == pd.DatetimeIndex
        eq_type = df['equipment_type'][0]
        potential_tags = [eq_type + '.' + col for col in tags]
        # make sure that each dataframe has at least 3 of the tags in the query specified
        assert len(set(df.columns) & set(potential_tags)) >= 2
        # timestamps (the index) returned should be in range queried
        for entry in df.index.values:
            entry_dt = parser.parse(str(entry))
            assert entry_dt > t1
            assert entry_dt < t2


@pytest.mark.skip(reason="athena queries cost money and are slow.")
def test_conveyor_sensor_query():
    """
    test grabbing all the sensors associated with a specific conveyor
    """
    S3BUCKET_NAME = 'mhspredict-site-fedex-louisville'
    conveyor_name = 'SS1-4'
    known_sensors = ['12', '13', '20', '14']

    aqe = data_retrieval.DataQueryEngine(s3_bucket_name=S3BUCKET_NAME)

    df = aqe.get_conveyor_sensors(conveyor_name)

    assert type(df) == type(pd.DataFrame())
    assert 'sensor' in df.columns
    assert 'conveyor' in df.columns
    assert 'equipment' in df.columns
    assert conveyor_name in df.conveyor.values
    assert 'PLC-{}'.format(conveyor_name) in df.sensor.values
    for known_sensor_val in known_sensors:
        assert known_sensor_val in df.sensor.values


@mock_s3
def test_sensor_location_query():
    """
    test sensor location query with s3 direct file read
    """

    bucket_name = 'mhspredict-testing'
    snumber = '444'

    # create s3 bucket with mock location file in it
    conn = boto3.resource('s3', region_name='us-east-1')

    conn.create_bucket(Bucket=bucket_name)
    s3_client = boto3.client('s3')
    data2store = 'sensor,description,manufacturer,conveyor,location,equipment,m_sensor,standards_score,temp_score,vvrms_score,thresholds\n444,Recirc 1,banner,SR1-8,M,M,10,1,1,1,`{"temperature": {"red": 140, "orange": 122}, "rms_acceleration": {"red": 0.37, "orange": 0.18, "blue": 0.07}}`\n'
    s3_client.put_object(Bucket=bucket_name, Body=data2store, Key='test/location/location_{}.csv'.format(snumber))

    aqe = data_retrieval.DataQueryEngine(s3_bucket_name=bucket_name)

    ldf = aqe.execute_location_query(sensor_name=snumber, gateway_name='test')

    assert type(ldf) == pd.DataFrame
    header_names = 'sensor,description,manufacturer,conveyor,location,equipment,m_sensor,standards_score,temp_score,vvrms_score,thresholds'
    for colname in ldf.columns:
        assert colname in header_names.split(",")

    assert ldf['sensor'][0] == int(snumber)
    # make sure jsons can be parsed appropriately
    assert type(json.loads(ldf['thresholds'][0])) == dict


@mock_s3
def test_sensor_stats_query():
    """
    test sensor stats query with s3 direct file read
    """
    bucket_name = 'mhspredict-testing'
    snumber = '449'

    # create s3 bucket with mock location file in it
    conn = boto3.resource('s3', region_name='us-east-1')
    conn.create_bucket(Bucket=bucket_name)
    s3_client = boto3.client('s3')
    data2store = "sensor,temperature_mean,temperature_perc95,rms_velocity_z_mean,rms_velocity_z_perc95\n449,0,0,0,0"
    s3_client.put_object(Bucket=bucket_name, Body=data2store, Key='test/stats/stats_table_{}.csv'.format(snumber))

    aqe = data_retrieval.DataQueryEngine(s3_bucket_name=bucket_name)

    sdf = aqe.execute_stats_table_query(sensor_name=snumber, gateway_name='test')

    header_names = "sensor,temperature_mean,temperature_perc95,rms_velocity_z_mean,rms_velocity_z_perc95"
    assert type(sdf) == pd.DataFrame
    for colname in sdf.columns:
        assert colname in header_names.split(",")
    assert sdf['sensor'][0] == int(snumber)


def test_relative_timestamp_construct():
    """
    test that we can construct tuple of relative timestsamps
    """

    tzoffset = -4
    tback = '1 hours'

    timetuple = data_retrieval.construct_relative_timestamp_tuple(
        time_back=tback,
        tzoffset=tzoffset
    )

    assert len(timetuple) == 2
    assert (timetuple[1] - timetuple[0]).total_seconds() == pytest.approx((Q_(tback).to('seconds')).magnitude)


def test_split_up_dfs_by_sensor():
    aqe = data_retrieval.DataQueryEngine()

    with open('tests/test_data_init/data_from_queries.pickle', 'rb') as input:
        data_from_queries = pickle.load(input)
    with open('tests/test_data_init/sensor_info_df.pickle', 'rb') as input1:
        sensors_info_df = pickle.load(input1)
    df = aqe._split_up_dfs_by_sensor(sensors_info_df, data_from_queries).values()
    assert pd.isnull(df) == False
    assert 'sensor' in list(df)[0].columns
    assert 'equipment_type' in list(df)[0].columns
    assert 'conveyor' in list(df)[0].columns
