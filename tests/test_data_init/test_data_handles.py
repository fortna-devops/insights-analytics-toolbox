#!/usr/bin/env python3
"""
Tests for the data handling module in data_init
..author: Oluwatosin Sonuyi <oluwatosinsonuyi@mhsinc.net>
"""
import pdb
import pytest
import pandas as pd
import numpy as np
from analytics_toolbox.data_init import data_handles


@pytest.fixture
def input_df_list():
    df_dict_src0 = {
        'tag10': [1, 2, 3, 4, 5],
        'tag20': [6, 7, 8, 9, 10],
        'tag30': [11, 12, 13, 14, 15]
    }

    df_dict_src1 = {
        'tag11': [0.1, 0.2, 0.3, 0.4, 0.5],
        'tag21': [0.6, 0.7, 0.8, 0.9, 0.10],
        'tag31': [0.11, 0.12, 0.13, 0.14, 0.15]
    }

    df_dict_src2 = {
        'tag12': [0.12, 0.22, 0.32, 0.42, 0.52],
        'tag22': [0.62, 0.72, 0.82, 0.92, 0.102],
        'tag32': [0.112, 0.122, 0.132, 0.142, 0.152]
    }

    df_dict_src3 = {
        'tag13': [0.13, 0.23, 0.33, 0.43, 0.53],
        'tag23': [0.63, 0.73, 0.83, 0.93, 0.103],
        'tag33': [0.113, 0.123, 0.133, 0.143, 0.153]
    }

    id0 = ["2018-06-11T13:52:58.532785-04:00",
           "2018-06-11T13:54:51.422717-04:00",
           "2018-06-11T13:56:29.940075-04:00",
           "2018-06-11T13:58:52.625784-04:00",
           "2018-06-11T14:00:29.445794-04:00"]

    id1 = ["2018-06-11T13:53:01.532785-04:00",
           "2018-06-11T13:54:59.422717-04:00",
           "2018-06-11T13:55:59.990075-04:00",
           "2018-06-11T13:59:01.625784-04:00",
           "2018-06-11T14:00:11.115794-04:00"]

    df0 = pd.DataFrame(df_dict_src0)
    df1 = pd.DataFrame(df_dict_src1)
    df2 = pd.DataFrame(df_dict_src2)
    df3 = pd.DataFrame(df_dict_src3)

    df0.index = pd.DatetimeIndex(id0)
    df1.index = pd.DatetimeIndex(id1)
    df2.index = pd.DatetimeIndex(id0)
    df3.index = pd.DatetimeIndex(id1)
    return [df0, df1, df2, df3]


def test_set_dt_index():
    df_d = {'read_time': ["2018-06-11T13:52:58.532785-04:00",
                          "2018-06-11T13:54:51.422717-04:00"],
            'tag1': [4, 7]}
    df = pd.DataFrame(df_d)
    dfnew = data_handles.set_datetime_index(df)
    assert isinstance(dfnew.index, pd.DatetimeIndex)
    assert 'read_time' not in dfnew.columns
    assert 'datetime' not in dfnew.columns


def test_set_dt_index_multiple():
    df_d = {'read_time': ["2018-06-11T13:52:58.532785-04:00",
                          "2018-06-11T13:54:51.422717-04:00"],
            'datetime': ["2018-06-11T13:52:58.532785-04:00",
                         "2018-06-11T13:54:51.422717-04:00"],
            'tag1': [4, 7]}
    df = pd.DataFrame(df_d)
    dfnew = data_handles.set_datetime_index(df)
    assert isinstance(dfnew.index, pd.DatetimeIndex)
    assert 'read_time' not in dfnew.columns
    assert 'datetime' not in dfnew.columns


def test_merge_2dfs(input_df_list):
    merged_df = data_handles.merge_disparate_dfs(input_df_list[0:2], merge_tol='60s')
    print(input_df_list[0:2])
    print(merged_df)
    print(merged_df.columns)
    assert len(merged_df.index) == 5
    assert 'tag10' in merged_df.columns
    assert 'tag20' in merged_df.columns
    assert 'tag30' in merged_df.columns
    assert 'tag11' in merged_df.columns
    assert 'tag21' in merged_df.columns
    assert 'tag31' in merged_df.columns
    assert len(merged_df.columns) == 6


def test_merge_3dfs(input_df_list):
    merged_df = data_handles.merge_disparate_dfs(input_df_list[0:3], merge_tol='60s')
    assert len(merged_df.index) == 5
    assert 'tag10' in merged_df.columns
    assert 'tag20' in merged_df.columns
    assert 'tag30' in merged_df.columns
    assert 'tag12' in merged_df.columns
    assert 'tag22' in merged_df.columns
    assert 'tag32' in merged_df.columns
    assert len(merged_df.columns) == 9


def test_merge_4dfs(input_df_list):
    merged_df = data_handles.merge_disparate_dfs(input_df_list, merge_tol='60s')
    assert len(merged_df.index) == 5
    assert 'tag10' in merged_df.columns
    assert 'tag20' in merged_df.columns
    assert 'tag30' in merged_df.columns
    assert 'tag13' in merged_df.columns
    assert 'tag23' in merged_df.columns
    assert 'tag33' in merged_df.columns
    assert len(merged_df.columns) == 12


def test_merge_df_wrong_datatype_input():
    with pytest.raises(TypeError) as t_error:
        data_handles.merge_disparate_dfs([1, 3, 4])
    assert t_error


def test_merge_df_zero_df_input(input_df_list):
    with pytest.raises(IndexError) as i_error:
        data_handles.merge_disparate_dfs([])
    assert i_error


def test_merge_df_one_df_input(input_df_list):

    df = data_handles.merge_disparate_dfs(input_df_list[0:1])
    assert [df] == input_df_list[0:1]
