#!/usr/bin/env python3

import pytest
import pandas as pd
import numpy as np

import pdb

# from analytics_toolbox.machine_learning import data_preparation, modeling
# from analytics_toolbox.preprocessing import data_cleaning

@pytest.fixture
def input_df():
	df_dict_src0 = {
			   'feat1':np.random.randn(20).tolist(),
			   'feat2':np.random.randn(20).tolist(),
			   'target':np.random.randn(20).tolist()
			   }

	df0 = pd.DataFrame(df_dict_src0)


	return df0

@pytest.mark.skip(reason="no longer has sklearn/scipy dependency")
def test_modeling_fit_predict_svr(input_df):


	motor_df, _ = data_preparation.scale_data(input_df)


	motor_df = motor_df.dropna(inplace=False)

	df_train, df_test = data_preparation.split_train_test(motor_df,train_ratio=3/5)


	features_train,target_train = data_preparation.separate_target(df_train,target_colnames=['target'])
	features_test,target_test = data_preparation.separate_target(df_test,target_colnames=['target'])

	est, res = modeling.SupportVectorMachineFitPredict(features_train,target_train,features_test,target_test)

	assert est.shape == res.shape
@pytest.mark.skip(reason="no longer has sklearn/scipy dependency")
def test_modeling_fit_predict_mlpr(input_df):


	motor_df, _ = data_preparation.scale_data(input_df)


	motor_df = motor_df.dropna(inplace=False)

	df_train, df_test = data_preparation.split_train_test(motor_df,train_ratio=3/5)



	features_train,target_train = data_preparation.separate_target(df_train,target_colnames=['target'])
	features_test,target_test = data_preparation.separate_target(df_test,target_colnames=['target'])

	est,res = modeling.MultilayerPerceptronFitPredict(features_train,target_train,features_test,target_test)

	assert est.shape == res.shape
@pytest.mark.skip(reason="no longer has sklearn/scipy dependency")
def test_model_trainer_object_creation_MLP(input_df):
	tr = 4/5.
	MT = modeling.ModelTrainer(input_df,'target',algorithm='MLP',train_ratio=tr)

	acts = MT.actuals['target'].iloc[int(len(input_df)*tr):].values
	ests = MT.estimates[0].values

	assert (MT.actuals==input_df).all().all()

	assert pytest.approx((acts-ests)) == MT.residuals[0].values

	assert type(MT.model_object) == MLPRegressor

@pytest.mark.skip(reason="no longer has sklearn/scipy dependency")
def test_model_trainer_object_creation_Gradient_Tree_Regression_Boosting(input_df):
	tr = 4/5.
	MT = modeling.ModelTrainer(input_df,'target',algorithm='GTBoost',train_ratio=tr)

	acts = MT.actuals['target'].iloc[int(len(input_df)*tr):].values
	ests = MT.estimates[0].values

	assert (MT.actuals==input_df).all().all()

	assert pytest.approx((acts-ests)) == MT.residuals[0].values

	assert type(MT.model_object) == GradientBoostingRegressor

@pytest.mark.skip(reason="no longer has sklearn/scipy dependency")
def test_model_trainer_prediction(input_df):
	pass










