#!/usr/bin/env python3

import pdb
import pytest
import pandas as pd
import numpy as np
#from analytics_toolbox.machine_learning import data_preparation, modeling

@pytest.fixture
def input_df():
	df_dict_src0 = {
			   'tag10':[1,2,3,4,5],
			   'tag20':[6,7,8,9,10],
			   'tag30':[11,12,13,14,15]
			   }

	df0 = pd.DataFrame(df_dict_src0)


	return df0

@pytest.fixture
def high_corr_df():
	tagdata = np.array([1,2,4,45,5,645,63,24,53,3])
	otherdata = np.array([21,43532,42,232,15,6145,633,2,8,13])

	src = {'tag1': tagdata,
		   'tag2': tagdata*2,
		   'tag3': otherdata,
		   'tag4': otherdata*3}
	return pd.DataFrame(src)

@pytest.mark.skip(reason="no longer has sklearn/scipy dependency")
def test_split_train_test(input_df):
	ratio = 2/5.
	train_df,test_df = data_preparation.split_train_test(input_df,ratio)
	assert len(train_df.index) == int(ratio*len(input_df.index))
	assert len(test_df.index) == int((1-ratio)*len(input_df.index))
@pytest.mark.skip(reason="no longer has sklearn/scipy dependency")
def test_split_train_test_shuffle_not_implemented(input_df):
	with pytest.raises(NotImplementedError) as ni_error:
		data_preparation.split_train_test(input_df,1/5.,True)
	assert ni_error
@pytest.mark.skip(reason="no longer has sklearn/scipy dependency")
def test_separate_target_one_col_as_string(input_df):
	# test that each column string can be set as the target and they will be split out
	for target_col in input_df.columns:
		features,target = data_preparation.separate_target(input_df,target_col)

		feature_cols_expected = set(input_df.columns) - set([target_col])
		target_cols_expected = set([target_col])

		featureset = set(features.columns)
		targetset = set([target.name])

		assert feature_cols_expected == featureset
		assert target_cols_expected == targetset
@pytest.mark.skip(reason="no longer has sklearn/scipy dependency")
def test_separate_target_one_col_as_list(input_df):
	# test that each column can be set as the target and they will be split out
	for target_col in input_df.columns:
		target_col = [target_col]
		features,target = data_preparation.separate_target(input_df,target_col)

		feature_cols_expected = set(input_df.columns) - set(target_col)
		target_cols_expected = set(target_col)

		featureset = set(features.columns)
		targetset = set(target.columns)

		assert feature_cols_expected == featureset
		assert target_cols_expected == targetset

@pytest.mark.skip(reason="no longer has sklearn/scipy dependency")
def test_separate_target_drop_corr_not_implemented(input_df):
	with pytest.raises(NotImplementedError) as ni_error:
		data_preparation.separate_target(input_df,'',True)
	assert ni_error
@pytest.mark.skip(reason="no longer has sklearn/scipy dependency")
def test_scaling(input_df):
	fmaxs = [1, 1, 93]
	fmins = [0,-1,-34]

	# test each pair of max/min has columns preserved
	# test each pair of max/min holds for every column 
	for feature_max,feature_min in zip(fmaxs,fmins):
		frange = (feature_min,feature_max)
		scaled, mm = data_preparation.scale_data(input_df,frange)
		assert set(scaled.columns) == set(input_df.columns)
		for col in scaled.columns:
			assert pytest.approx(max(scaled[col])) == feature_max
			assert pytest.approx(min(scaled[col])) == feature_min
@pytest.mark.skip(reason="no longer has sklearn/scipy dependency")
def test_scaling_with_nans():
	df_dict_src0 = {
			   'tag10':[1,2,3,4,5],
			   'tag20':[6,np.nan,8,9,10],
			   'tag30':[11,12,13,14,np.nan]
			   }

	input_df = pd.DataFrame(df_dict_src0)

	fmaxs = [1, 1, 93]
	fmins = [0,-1,-34]

	# test each pair of max/min has columns preserved
	# test each pair of max/min holds for every column 
	for feature_max,feature_min in zip(fmaxs,fmins):
		frange = (feature_min,feature_max)
		scaled, mm = data_preparation.scale_data(input_df,frange)
		assert set(scaled.columns) == set(input_df.columns)
		for col in scaled.columns:
			assert pytest.approx(max(scaled[col])) == feature_max
			assert pytest.approx(min(scaled[col])) == feature_min
@pytest.mark.skip(reason="no longer has sklearn/scipy dependency")
def test_inverse_scaling_no_params(input_df):

	scaled_df, _ = data_preparation.scale_data(input_df)
	with pytest.raises(ValueError) as verror:
		data_preparation.inverse_scale_data(scaled_df)
	assert verror
@pytest.mark.skip(reason="no longer has sklearn/scipy dependency")
def test_inverse_scaling_with_minmaxscaler(input_df):

	scaled_df, scaler = data_preparation.scale_data(input_df)

	new_df = data_preparation.inverse_scale_data(scaled_df,minmaxscaler=scaler)

	assert np.allclose(new_df,input_df)
@pytest.mark.skip(reason="no longer has sklearn/scipy dependency")
def test_inverse_scaling_with_original_df(input_df):

	scaled_df, _ = data_preparation.scale_data(input_df)

	new_df = data_preparation.inverse_scale_data(scaled_df,original_df=input_df)

	assert np.allclose(new_df,input_df)
@pytest.mark.skip(reason="no longer has sklearn/scipy dependency")
def test_end_to_end_inverse_scale(input_df):

	target = 'tag30'
	motor_df, mms = data_preparation.scale_data(input_df)
	tr = 3/5.
	df_train, df_test = data_preparation.split_train_test(motor_df,train_ratio=tr)



	features_train,target_train = data_preparation.separate_target(df_train,target_colnames=[target])
	features_test,target_test = data_preparation.separate_target(df_test,target_colnames=[target])

	# model training
	# MultilayerPerceptronFitPredict
	est,res = modeling.MultilayerPerceptronFitPredict(features_train,target_train,features_test,target_test)

	est_scaled = data_preparation.inverse_scale_data(df_to_transform=est,original_df=input_df[target])
	true_scaled = data_preparation.inverse_scale_data(df_to_transform=target_test,original_df=input_df[target])

	idx = int(len(input_df[target])*tr)

	assert np.allclose(true_scaled,input_df[target][idx:].values.reshape(-1,1))

@pytest.mark.skip(reason="no longer has sklearn/scipy dependency")
def test_drop_highly_correlated_columns(high_corr_df):
	corr_limit = 0.93
	new_df = data_preparation.drop_high_correlations(high_corr_df,corr_limit=corr_limit)
	corr_matrix = new_df.corr()
	# only take the upper columns
	upper = corr_matrix.where(np.triu(np.ones(corr_matrix.shape), k=1).astype(np.bool))
	for l in upper.values:
		for x in l:
			if not np.isnan(x):
				assert x < corr_limit


	# assert that half the columns get dropped
	assert len(new_df.columns) == 0.5*(len(high_corr_df.columns))









