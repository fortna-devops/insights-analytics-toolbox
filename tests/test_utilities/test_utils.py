#!/usr/bin/env python3

import pdb
import pytest
import pandas as pd
import numpy as np
from analytics_toolbox.utilities import utils

@pytest.fixture
def truth_series_200():

	x = [True]*200 + [False] + [True]*20 + [False]*10 + [True]*30
	return pd.DataFrame(x)

@pytest.fixture
def truth_series_20_20():

	x = [True]*20
	y = [False]*20

	return pd.DataFrame(x+y)

@pytest.fixture
def truth_series_2_2():
	x = [True,True]
	y = [False,False]
	return pd.DataFrame(x+y+x+y+x+y)

@pytest.fixture
def truth_series_1_1():
	x = [True,False,True,False,True,False,True,False]
	return pd.DataFrame(x)

@pytest.fixture
def truth_series_0_30():
	x = [False]*30
	return pd.DataFrame(x)

def test_persistence_check_all_false(truth_series_0_30):
	"""
	checks the persistence check fails
	when all inputs are false
	"""
	for x in range(1,35):
		meets_pers = utils.check_persistence(truth_series_0_30,x)
		assert meets_pers is False

def test_persistence_check_all_false(truth_series_0_30):
	"""
	checks the persistence check passes
	when all inputs are false but persistence is 0
	"""
	pers = 0
	meets_pers = utils.check_persistence(truth_series_0_30,pers)
	assert meets_pers is True

def test_persistence_check_3_and_up(truth_series_2_2):
	"""
	checks the persistence check fails when we
	are looking for more than the number
	of consecutive truths than exist in the series
	"""
	for x in range(3,10):
		meets_pers = utils.check_persistence(truth_series_2_2,x)
		assert meets_pers is False


def test_persistence_check_3_and_up(truth_series_2_2):
	"""
	checks the persistence check passes when we
	are looking for less than the number
	of consecutive truths than exist in the series
	"""
	for x in range(3):
		print(x)
		meets_pers = utils.check_persistence(truth_series_2_2,x)
		assert meets_pers is True

def test_persistence_check_20_and_down(truth_series_20_20):
	"""
	checks the persistence check passes when we
	are looking for exactly the number of consecutive truths 
	or less than the number of consecutive truths
	"""
	pers = 20
	for x in range(pers+1):
		print(x)
		meets_persistence = utils.check_persistence(truth_series_20_20,x)
		assert meets_persistence is True

def test_persistence_check_20_and_up(truth_series_20_20):
	"""
	checks the persistence check fails when we
	are looking for more than the number of consecutive truths present
	"""
	for x in range(21,41):
		meets_persistence = utils.check_persistence(truth_series_20_20,x)
		assert meets_persistence is False

def test_persistence_check_mix_match_true(truth_series_200):
	"""
	checks the persistence check passes when we
	are looking for up to the number of consecutive truths present
	"""
	for x in range(201):
		print(x)
		meets_persistence = utils.check_persistence(truth_series_200,x)
		assert meets_persistence is True

def test_persistence_check_mix_match_false(truth_series_200):
	"""
	checks the persistence check failse when we
	are looking for more than the number of consecutive truths present
	"""
	for x in range(201,230):
		meets_persistence = utils.check_persistence(truth_series_200,x)
		assert meets_persistence is False

def test_persistence_check_1_1(truth_series_1_1):
	"""
	checks the persistence check fails when we
	are looking for more than the number of consecutive truths present
	"""
	for x in range(2,20):
		meets_persistence = utils.check_persistence(truth_series_1_1,x)
		assert meets_persistence is False

def test_persistence_check_1_1_true(truth_series_1_1):
	"""
	checks the persistence check passes when we
	are looking for 1 consecutive truth
	"""
	pers = 1
	meets_persistence = utils.check_persistence(truth_series_1_1,pers)
	assert meets_persistence is True

def test_persistence_check_empty():
	"""
	checks that persitence check false when input is empty
	"""

	meets_pers = utils.check_persistence(pd.DataFrame([]),5)
	assert meets_pers is False
