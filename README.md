# Analytics Toolbox

This library contains a series of functions, objects, and tools to be used across all analytics development. Standardized implementation of multiple common functions and operations will be contained in various modules. Will be available for usage within the AWS environment

i.e.

`from analytics_toolbox import preprocessing.range_check`

Currently planned functions & objects:

- data querying

- tag & tag container object to implement more advanced analytics & better readability

- units of measure management to ensure accuracy and extensibility

- range checks to filter out data when a certain signal is in a particular range

- outlier filtering to throw out data outside of a number of standard deviations


*under development*

To install:

Activate the environment you would like to install the toolbox into, run the follwing line:
`python setup.py install`